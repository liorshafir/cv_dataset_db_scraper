import os
from recording_fs_db_scrapper import FileSystemDbScraper
from mysql_db_manager import MySQLManager
from detection_dir_process import DetectionDirProcess
from dir_process import DirProcess
from datetime import date
from datetime import datetime
import smtplib
import scraper_config as cfg
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(email_content, recordings, updates, deleted_folders):
    #workaround to not inform on deleted_folders:
    deleted_folders = []

    sender_email = "liors@innoviz-tech.com"
    receiver_email = "liors@innoviz-tech.com"
    message = MIMEMultipart("alternative")
    message["Subject"] = "DB Update - Test please ignore"
    message["From"] = sender_email
    message["To"] = receiver_email
    text = """\
    Plain text message
    """
    full_template_path = os.path.join(os.getcwd(), 'html_templates\\annotation-status.html')
    with open(full_template_path, 'r') as template_file:
        html = template_file.read()
    directories_html_path = os.path.join(os.getcwd(), 'html_templates\\directoriestable.html')
    with open(directories_html_path, 'r') as table1_file:
        dir_table = table1_file.read()
    deleted_directories_html_path = os.path.join(os.getcwd(), 'html_templates\\deleted-directories.html')
    with open(deleted_directories_html_path, 'r') as deleted_directories_file:
        deletedrec_table = deleted_directories_file.read()
    filechanges_html_path = os.path.join(os.getcwd(), 'html_templates\\filechanges.html')
    with open(filechanges_html_path, 'r') as changes_file:
        changes_table = changes_file.read()
    marked_for_annotation_html_path = os.path.join(os.getcwd(), 'html_templates\\markedannotationchanges.html')
    with open(marked_for_annotation_html_path, 'r') as marked_annotation_file:
        marked_table = marked_annotation_file.read()

    html = html.replace("#time", datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    if len(recordings) == 0:
        html = html.replace("#RecordingsTable", "None")
    else:
        table_body = ""
        for rec in recordings:
            table_body = table_body + "<tr><td>" + rec + "</td></tr>"
        dir_table = dir_table.replace("#typetbody", table_body)
        html = html.replace("#RecordingsTable", dir_table)

    if len(deleted_folders) == 0:
        html = html.replace("#DeletedRecTable", "None")
    else:
        deleted_table_body = ""
        for delrec in deleted_folders:
            deleted_table_body = deleted_table_body + "<tr><td>" + delrec + "</td></tr>"
        deletedrec_table = deletedrec_table.replace("#typedeletedbody", deleted_table_body)
        html = html.replace("#DeletedRecTable", deletedrec_table)

    changes_body = ""
    for file_change in updates:
        if file_change["created"] > 0 or file_change["updated"] > 0 or file_change["invalidated"] > 0:
            changes_body = changes_body + "<tr><td style=\"width:600px; max-width: 600px;\">" + file_change["path"] + "</td><td style=\"text-align:center;\">" + str(file_change["created"]) + "</td><td style=\"text-align:center;\">" + str(file_change["updated"]) + "</td><td style=\"text-align:center;\">" + str(file_change["invalidated"]) + "</td></tr>"
    changes_table = changes_table.replace("#filechanges", changes_body)

    changes_body = ""
    for file_change in updates:
        if file_change["marked_for_annotation"] > 0:
            changes_body = changes_body + "<tr><td style=\"width:600px; max-width: 600px;\">" + file_change["path"] + "</td><td style=\"text-align:center;\">" + str(file_change["marked_for_annotation"]) + "</td></tr>"
    marked_table = marked_table.replace("#markedchanges", changes_body)


    html = html.replace("#ChangesTable", changes_table)
    html = html.replace("#MarkedForAnnotationTable", marked_table)

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)
    server = smtplib.SMTP('mailgw.il.innoviz.tech', port=25)
    server.ehlo
    server.sendmail(sender_email, ['liors@innoviz-tech.com'], message.as_string())
    #server.sendmail(sender_email, ['liors@innoviz-tech.com', 'amird@innoviz-tech.com', 'orens@innoviz-tech.com', 'liatr@innoviz-tech.com',  'michael.hustochk@innoviz-tech.com'], message.as_string())
    #server.sendmail('liors@innoviz-tech.com',
    #                ['amird@innoviz-tech.com', 'orens@innoviz-tech.com', 'liatr@innoviz-tech.com',
    #                 'michael.hustochk@innoviz-tech.com', 'liors@innoviz-tech.com'], email_content)
    #server.sendmail('liors@innoviz-tech.com',
    #                ['liors@innoviz-tech.com'], email_content)


def generate_summary_msg(new_recordings, updates, deleted_folders):
    are_changes = False
    summary_msg = "Summary:\n"
    if len(new_recordings) > 0:
        summary_msg = summary_msg + "New discovered recording paths: \n"
    for result in new_recordings:
        summary_msg = summary_msg + result + "\n"
    if len(deleted_folders) > 0:
        summary_msg = summary_msg + "==============================================\nDeleted recording paths:\n"
    for deleted_folder in deleted_folders:
        summary_msg = summary_msg + deleted_folder + "\n"
    if len(new_recordings) > 0:
        summary_msg = summary_msg + "==============================================\nObject Detection .json files updates:\n"
    for result in updates:
        if result["created"] > 0 or result["updated"] > 0 or result["invalidated"] > 0 or result["marked_for_annotation"]:
            are_changes = True
            summary_msg = summary_msg + result["path"] + ":" + "\n"
            summary_msg = summary_msg + "  Created:" + str(result["created"]) + " frames" + "\n"
            summary_msg = summary_msg + "  Updated:" + str(result["updated"]) + " frames" + "\n"
            summary_msg = summary_msg + "  Invalidated:" + str(result["invalidated"]) + " frames" + "\n"
            summary_msg = summary_msg + "  Marked for annotation:" + str(result["marked_for_annotation"]) + " frames" + "\n"
    if not are_changes:
        summary_msg = summary_msg + " No changes"
    return summary_msg

if __name__ == '__main__':
    _db_mngr = MySQLManager()
    _db_mngr.connect()
    # This line calls the constructor of the FileSystemDbScraper in recording_fs_db_scrapper.py - should FIX that; a bit of mess in code.
    roots = FileSystemDbScraper(_db_mngr)

    # Get the possible statuses from the DB and get the strategies for the recordings.
    _db_mngr.get_static_metadata()
    dataset_root_folder = cfg.dataset_root_folder
    all_invz_folders = []
    detection_gt_files = roots.run_detections(dataset_root_folder, all_invz_folders)
    results = []
    recording_results = []

    for gt_pair in detection_gt_files:
        # Check if the recording sensor suite already exists and get its id
        sensor_suite_id = -1
        for item in roots.existing_recording_paths:
            if gt_pair['dir'] == item[1]:
                sensor_suite_id = item[0]
        if sensor_suite_id == -1:
            sensor_suite_id = DirProcess(gt_pair['dir'], _db_mngr, roots.log).run()
            roots.existing_recording_paths.append((sensor_suite_id, gt_pair['dir']))
            recording_results.append(gt_pair['dir'])
        if gt_pair['is_json']:
            results.append(DetectionDirProcess(sensor_suite_id, os.path.join(gt_pair['dir'], gt_pair['file']), _db_mngr, roots.log).run())
            roots.update_gt_file_tracking(os.path.join(gt_pair['dir'], gt_pair['file']))

    # Check which recording directories were collected in the DB but no longer exist
    deleted_folders = []
    for db_rec_session in roots.existing_recording_paths:
        is_deleted = True
        for existing_folder in all_invz_folders:
            if existing_folder == db_rec_session[1]:
                is_deleted = False
        if is_deleted:
            deleted_folders.append(db_rec_session[1])
            _db_mngr.update_deleted_sensor_suite(db_rec_session[0])

    today = date.today()
    summary_msg = generate_summary_msg(recording_results, results, deleted_folders)
    email_msg = "Subject: CV DB Update " + today.strftime("%d/%m/%Y") + "\n\n    "
    email_msg = email_msg + summary_msg
    send_email(email_msg, recording_results, results, deleted_folders)
    roots.log(summary_msg)

    #orens_slack_token = 'xoxp-19517798279-237086150917-852513661541-bcbca1856620eec76cb260a61d6e77fc'
    #slack_channel = 'cv_bot_data'
    #client = slack.WebClient(token=orens_slack_token)
    #response = client.chat_postMessage(channel=slack_channel, text=msg)
