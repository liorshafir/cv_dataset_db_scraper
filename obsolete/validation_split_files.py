import json
import mysql.connector
from obsolete import logger_utils as logu
from logging import ERROR

if __name__ == '__main__':
    logger = logu.init_logger('fs_db_scraper', log_path='./fs_db_scraper.log')
    try:
        with open("C:\\Work\DBProject\\Oren_split_file\\default_splits_object_detection.json", 'r') as f:
            json_data = json.load(f)  # type: dict
    except:
        logger.log(ERROR, "failed to open split file")
    if "validation" in json_data.keys():
        listLines = []
        for split_line in json_data['validation']:
            listLines.append(split_line.replace("/", "\\").replace("\\mnt\\weka01", "\\\\inv-weka01-smb.il.innoviz.tech"))

        mydb = mysql.connector.connect(
            host='127.0.0.1',
            database='cv_dev',
            user='root',
            password='li1980or',
            auth_plugin='mysql_native_password'
        )

        mycursor = mydb.cursor()
        mycursor.execute("SELECT rmd.path, ses.id from raw_module_data rmd INNER JOIN sensor_suites ses ON rmd.sensor_suite_id = ses.id "
                         "WHERE rmd.path like '%.invz%'")
        myresult = mycursor.fetchall()
        sesidList = []
        for res in myresult:
            for line in listLines:
                if res[0] in line:
                    sesidList.append(res[1])
        sesidList = list(set(sesidList))
        for sesId in sesidList:
            mycursor = mydb.cursor()
            mycursor.execute("INSERT into od_sensor_suite_cv_status (sensor_suite_id, cv_status_id) values ( " + str(sesId) + ", 1)")
            mydb.commit()


