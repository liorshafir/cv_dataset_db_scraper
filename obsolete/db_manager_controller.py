from enum import Enum
import http.client
import ssl
import json

from logging import DEBUG, INFO, WARNING, ERROR, FATAL


def log_default(msg, level):
    print(f"[{level}] msg")


class Methods(Enum):
    GET = 0
    POST = 1
    DELETE = 2
    PUT = 3


class DbMngrConnectException(RuntimeError):
    pass


class DBManagerController:

    def __init__(self, log_cbk=None):
        #for evitate this Error : ssl.SSLError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:833)
        ssl._create_default_https_context = ssl._create_unverified_context # TODO add remark explaining or remove
        #self.conn = http.client.HTTPConnection('10.71.100.124', 5000) #TODO change to local or by env configuration file
        self.conn = http.client.HTTPSConnection('localhost', 44302)

        # set log callback
        if log_cbk is None:
            self._log_cbk = log_default
        else:
            self._log_cbk = log_cbk

    def connect(self):
        try:
            self.conn.connect()
        except http.client.HTTPException as e:
            raise DbMngrConnectException(f"Connection failes, Error: {e}")

    def log(self, msg, level):
        self._log_cbk(msg, level)

    def request(self, url, method, params):
        '''
        send request , validate and return response
        :param url:
        :param method:
        :param params:
        :return:
        '''

        # define header
        headers = ({"Content-type": "application/json"})

        # send request
        self.log_get_request(method, url, headers)
        self.conn.request(method.name, url, params, headers)

        # get response
        try:
            res = self.conn.getresponse()
        except http.client.HTTPException as e:
            self.log(e.args, FATAL)
            raise

        try:
            response_byte = res.read()
        except Exception as e:
            # validate obj_name exist
            self.log(e.args, WARNING)
        return response_byte

    def log_get_request(self, method, url, headers):
        self.log(f"request: method: '{method}', url: '{self.conn.host}{url}', headers: {headers}", level=DEBUG)

    def create_obj(self, url, method, params, obj_name, obj_table):
        # send request
        response_byte = self.request(url, method, params)


        # check for errors
        self.check_for_errors(response_byte)

        # read the response

        response_json = json.loads(response_byte)
        # get object from response
        obj = response_json[obj_table][obj_name]

        return obj

    def check_for_errors(self, response_byte):
        print(response_byte)
        # validate success exists in response
        try:
            response_json = json.loads(response_byte)
        except:
            my_json = response_byte.decode('utf8').replace("'", '"')
            response_json = json.loads(my_json)


        success = response_json["success"]
        if success:
            pass
        else:
            self.log(response_json["message"], WARNING)
        #
        # if res.status != 200:
        #     msg = f"invalid response: status: '{res.status}', reason: {res.reason}"
        #     self.log(msg, level=FATAL)
        #     raise RuntimeError(msg)

        # validate success it true

   # def close(self):
    #    conn.close()

