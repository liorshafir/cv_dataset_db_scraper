import xlrd

from obsolete import logger_utils as logu


class AnnotationExcelScraper:
    def __init__(self):
        # define logger
        self._logger = logu.init_logger('fs_db_scraper', log_path='./fs_db_scraper.log')
        self.i = 0

    def extract_excel_rows(self, loc):
        wb = xlrd.open_workbook(loc)
        sheet = wb.sheet_by_index(0)

        root_folder_strs = ["innoviz_dataset\\_2018_07_new_mpu", "innoviz_dataset_v1\\_2018_07_new_mpu"]

        paths = []
        for i in range(sheet.nrows):
            isDataRow = False
            sheetVal = sheet.cell_value(i, 0)
            if type(sheetVal) is str:
                for activeRoot in root_folder_strs:
                    if activeRoot in sheetVal:
                        isDataRow = True
                if isDataRow:
                    # single_paths = sheetVal.split(' ')
                    # if (len(single_paths) != 1 and len(single_paths) != 2):
                    #    print('Error in data row: ' + sheetVal)
                    # for single_path in single_paths:
                    paths.append(sheetVal.replace('U:\\innoviz_dataset',
                                                  '\\\\inv-weka01-smb.il.innoviz.tech\\annotation\\innoviz_dataset'))
        return paths
