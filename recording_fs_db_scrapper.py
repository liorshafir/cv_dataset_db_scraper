import os
import re
from datetime import datetime
from datetime import timedelta
from mysql_db_manager import MySQLManager
from dir_process import DirProcess
import json
import hashlib

from logging import INFO, FATAL

from obsolete import logger_utils as logu

__version__ = '0.0.0'


class FileSystemDbScraper:
    def __init__(self, db_mngr):
        # define logger
        self.db_mngr = db_mngr
        self._logger = logu.init_logger('fs_db_scraper', log_path='./fs_db_scraper.log')
        self.i = 0
        existing_files = db_mngr.get_existing_gt_files()
        self.existing_gt_files = {}
        for existing_file in existing_files:
            self.existing_gt_files[existing_file[0]] = existing_file
        ignoreListFile = open("\\\\inv-weka01-smb.il.innoviz.tech\\annotation\\AnnotationToolShared\\ignoreList.txt",
                              "r")
        ignoreLines = ignoreListFile.readlines()
        self.ignoreLines = [i.rstrip() for i in ignoreLines]
        self.existing_recording_paths = db_mngr.get_existing_recording_paths()


    def log(self, msg, level=INFO):
        self._logger.log(level, msg)

    def update_gt_file_tracking(self, path):
        is_update = False
        calc_hash = self.calculate_gt_data_hash(path)
        modified = datetime.fromtimestamp(os.path.getmtime(path)).strftime('%Y-%m-%d %H:%M:%S')
        if path in self.existing_gt_files.keys():
            is_update = True
        if (is_update):
            self.db_mngr.update_file_tracking(path, modified, calc_hash)
        else:
            self.db_mngr.create_file_tracking(path, modified, calc_hash)


    def check_recording_condition_timestamp(self, path, directory, list, i):
        try:
            directory_list = os.listdir(path)
        except:
            self.log("The network path not found: "+str(path), FATAL)
            raise

        file = False
        for directory_sub in directory_list:
            if os.path.isfile(path + '/' + directory_sub) :
                file = True
                if re.search(r'\d\d\d\d\d\d\d\dT\d\d\d\d\d\d', directory.__str__()):
                    list.append(path)
                    break
        if not file or i == 0:
            i += 1
            for directory_sub in directory_list:
                if not os.path.isfile(path + '/' + directory_sub):
                    self.check_recording_condition_timestamp(path+"/"+directory_sub, directory_sub, list, i)
        return list

    def check_recording_condition_type(self, file_path, list):
        self.i = 0

        for dirpath, dirnames, files in os.walk(top=file_path):
            if files:
                for file in files:
                    if file.endswith(".invz"):
                        list.append(dirpath)
                        dirnames[:] = []
                        break
        return list

    def calculate_gt_data_hash(self, filepath):
        openedFile = open(filepath)
        readFile = openedFile.read()

        sha1Hash = hashlib.sha1(readFile.encode('utf-8'))
        sha1Hashed = sha1Hash.hexdigest()
        return sha1Hashed


    def check_detection_folder(self, file_path, list, all_invz_folders):
        self.i = 0

        for dirpath, dirnames, files in os.walk(top=file_path):
            if files:
                is_gt = False
                is_invz_pro = False
                is_invz_one = False
                for file in files:
                    if file.endswith(".invz"):
                        is_invz_pro = True
                for dirname in dirnames:
                    if dirname.endswith("invz4_3"):
                        is_invz_one = True
                if is_invz_pro or is_invz_one:
                    all_invz_folders.append(dirpath)
                    for file in files:
                        if file.endswith(".json") and not file.startswith("calibration") and not file.startswith("metadata") and not file.startswith("ego_motion"):
                            full_path = os.path.join(dirpath, file)
                            existing_item = tuple()
                            is_update = False
                            if full_path in self.existing_gt_files.keys():
                               existing_item = self.existing_gt_files[full_path]
                            if len(existing_item) == 0:
                                if not full_path in self.ignoreLines:
                                    is_update = True
                            else:
                                is_gt = True
                                if existing_item[1] is None or datetime.fromtimestamp(os.path.getmtime(full_path)) > existing_item[1] + timedelta(seconds=1):
                                    try:
                                        calc_hash = self.calculate_gt_data_hash(full_path)
                                        if (calc_hash != existing_item[2]):
                                           is_update = True
                                        else:
                                           self.db_mngr.update_file_tracking(full_path, datetime.fromtimestamp(os.path.getmtime(full_path)).strftime('%Y-%m-%d %H:%M:%S'), calc_hash)
                                    except:
                                        self.log("error opening json {}".format(full_path))
                            if is_update:
                                try:
                                    with open(full_path, 'r') as f:
                                        json_data = json.load(f)  # type: dict
                                except:
                                    #self.db_mngr.insert_ignore_file(full_path)
                                    self.log("error opening json {}".format(full_path))
                                if 'AnnotatedFrames' in json_data:
                                    is_gt = True
                                    list.append({ 'dir': dirpath,
                                                  'file': file,
                                                  'is_json': True})
                    if not is_gt:
                        list.append({'dir': dirpath,
                                     'file': '',
                                     'is_json': False})
        return list

    def run_recordings(self, root_folders):
        self.log(f"File system db scraper {__version__}")
        self.log("Connect to data base manager")

        list = []
        for root_folder in root_folders:
            self.check_recording_condition_type(root_folder, list)
        return list

    def run_detections(self, root_folders, all_invz_folders):
        self.log(f"File system db scraper {__version__}")
        self.log("Connect to data base manager")

        list = []
        for root_folder in root_folders:
            self.check_detection_folder(root_folder, list, all_invz_folders)
        return list


    def returnDate(self, dir_path):
        x = []
        x = dir_path.split('/')
        dir = x[-1]
        y = dir.split('T')
        year = y[0][0:4]
        month = y[0][4:6]
        day = y[0][6:8]

        hour = y[1][0:2]
        minute = y[1][2:4]
        second = y[1][4:6]
        return datetime(int(year), int(month), int(day), int(hour), int(minute), int(second))


if __name__ == '__main__':
    root = FileSystemDbScraper()
    _db_mngr = MySQLManager(root.log)
    _db_mngr.connect()
    recordings_root_folders = ['\\\\inv-weka01-smb.il.innoviz.tech\\recordings\\Recordings\\']
    dir_list = root.run_recordings(recordings_root_folders)
    #dir_list = root.run()
    for dir_path in dir_list:
        DirProcess(dir_path, _db_mngr, root.log).run()
        root.log("Processed successfully: " + dir_path)
    print(dir_list.__len__())
