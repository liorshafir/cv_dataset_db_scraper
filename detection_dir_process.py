import json
import hashlib
from datetime import date
from datetime import datetime
from datetime import timedelta
from logging import INFO


def log_default(msg, level):
    print(f"[{level}] [{msg}]")


class Frame:
    def __init__(self, modified_date, created_date, hash, status, strategy, gt_path, sensor_suite_id, frame_number, annotation_source):
        self.modified_date = modified_date
        self.created_date = created_date
        self.hash = hash
        self.status = status
        self.strategy = strategy
        self.gt_path = gt_path
        self.sensor_suite_id = sensor_suite_id
        self.frame_number = frame_number
        self.annotation_source = annotation_source

        self.total_num_objects = 0
        self.pedestrian = 0
        self.car = 0
        self.van = 0
        self.cyclist = 0
        self.truck = 0
        self.tram = 0
        self.person = 0
        self.misc = 0
        self.ignore = 0
        self.noise = 0
        self.motorcycle = 0
        self.animal = 0
        self.motorcyclist = 0
        self.bicycle = 0
        self.wheelchair = 0
        self.stroller = 0
        self.trailer = 0
        self.scooter = 0
        self.kalnoit = 0
        self.ghost = 0
        self.unknown = 0
        self.edgecase = 0
        self.sittingperson = 0

        self.type_to_str = {
          0: "pedestrian",
          1: "car",
          2: "van",
          3: "cyclist",
          4: "truck",
          5: "tram",
          6: "person",
          7: "misc",
          8: "ignore",
          9: "noise",
          10: "motorcycle",
          11: "animal",
          12: "motorcyclist",
          13: "bicycle",
          14: "wheelchair",
          15: "stroller",
          16: "trailer",
          17: "scooter",
          18: "kalnoit",
          19: "ghost",
          20: "unknown",
          21: "edgecase",
          22: "sittingperson",
        }

class DetectionDirProcess:

    def __init__(self, sensor_suite_id, gt_path, db_mngr, log_cbk=None):
        self.gt_path = gt_path
        self.db_mngr = db_mngr
        self.directory_list = None
        self.sensor_suite_id = sensor_suite_id

        if log_cbk is None:
            self._log_cbk = log_default
        else:
            self._log_cbk = log_cbk

        try:
            with open(gt_path, 'r') as f:
                self.gt_data_dict = json.load(f)  # type: dict
            self.is_bad_json = False
        except:
            self.is_bad_json = True
            #self.log("error opening json {}".format(gt_path))

        self.hash = None
        self.annotation_source = None

        self.strategy = None

    def log(self, msg, level=INFO):
        self._log_cbk(msg, level)

    def run(self):
        result = { "path": self.gt_path, "created": 0, "updated": 0, "invalidated": 0, "marked_for_annotation": 0}
        if self.is_bad_json:
            return result
        # find the matching recording
        self.hash = self.calculate_gt_data_hash()
        self.frames = self.process_frames()
        self.existing_frames = self.get_existing_frames()
        gt_file_frame_list = []
        for frame in self.frames.values():
            frame_number = frame.frame_number
            gt_file_frame_list.append(frame_number)
            if frame_number in self.existing_frames.keys():
                #self.log(self.gt_path + ": Updating frame " + str(frame_number))
                ex_frame = self.existing_frames[frame_number]
                frame_obj_id = ex_frame['frame_id']
                if frame.status != ex_frame['status_id']:
                    self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "FRAME_UPDATE_STATUS", frame_obj_id, None, "Frame " + str(frame_number) + ": changed from \'" + self.db_mngr.statusesDict[ex_frame['status_id']] + "\' to \'" +  self.db_mngr.statusesDict[frame.status] + "\'")
                    result["updated"] = result["updated"] + 1
                if frame.strategy != ex_frame['strategy_id']:
                    self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "FRAME_UPDATE_STRATEGY", frame_obj_id, None, "Frame " + str(frame_number) + ": changed strategy ")
                    result["updated"] = result["updated"] + 1
                if frame.modified_date > (ex_frame['modified_date']+ timedelta(seconds=1)).strftime('%Y-%m-%dT%H:%M:%S') or frame.total_num_objects != ex_frame['total_num_objects']:
                    self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "FRAME_UPDATE_TRACKLETS", frame_obj_id, None, "Frame " + str(frame_number) + ": tracklets change")
                    result["updated"] = result["updated"] + 1
                self.update_frame(frame_obj_id, frame)
            else:
                #self.log(self.gt_path + ": Creating frame " + str(frame_number))
                frame_obj_id = self.create_frame(frame)
                if frame.status == 1 or frame.status == 2:
                    self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
                                                      "FRAME_MARK_ANNOTATION", frame_obj_id, None,
                                                      "New frame was marked for annotation: " + str(frame_number))
                    result["marked_for_annotation"] = result["marked_for_annotation"] + 1
                else:
                    self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
                                                      "FRAME_CREATE", frame_obj_id, None,
                                                      "New frame: " + str(frame_number))
                    result["created"] = result["created"] + 1
            self.create_frame_status(frame, frame_obj_id)
        for old_frame in self.existing_frames.keys():
            if old_frame not in gt_file_frame_list:
                #self.log(self.gt_path + ": Invalidating frame " + str(old_frame))

                self.mark_frame_deleted(self.existing_frames[old_frame]['frame_id'])
                self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
                                                  "FRAME_DELETE", self.existing_frames[old_frame]['frame_id'], None,
                                                  "Frame " + str(old_frame) + ": removed from gt file")
                result["invalidated"] = result["invalidated"] + 1
        return result

    def find_corresponding_recording(self):
        return -1

    def compare_strategy_value(self, val1, val2):
        if not ',' in val1:
            return val1.upper() == val2.upper()
        else:
            return len(set(val1.upper().split(",")) & set(val2.upper().split(","))) == len(val1.upper().split(","))

    # if ke.upper() in [x.upper() for x in self.db_mngr.strategy_items.keys()]:
    def get_strategy_id(self, gt_file_strategy):
        keys_ids = []
        for ke in gt_file_strategy.keys():
            is_item_exist = False
            for ex_id in self.db_mngr.strategy_items.keys():
                if ke.upper() == self.db_mngr.strategy_items[ex_id]['key'].upper():
                    if self.compare_strategy_value(gt_file_strategy[ke], self.db_mngr.strategy_items[ex_id]['value']):
                        keys_ids.append(ex_id)
                        is_item_exist = True
                        break
            if not is_item_exist:
                new_strategy_item_id = self.db_mngr.insert_strategy_item(ke, gt_file_strategy[ke])
                keys_ids.append(new_strategy_item_id)
                self.db_mngr.strategy_items[new_strategy_item_id] = {
                    'key': ke,
                    'type': "Unknown",
                    'value': gt_file_strategy[ke]
                }
        for gr in self.db_mngr.strategy_groups.keys():
            if len(set(self.db_mngr.strategy_groups[gr]) & set(keys_ids)) == len(keys_ids):
                return gr
        new_strategy_group_id = self.db_mngr.insert_strategy_group(keys_ids)
        self.db_mngr.strategy_groups[new_strategy_group_id] = keys_ids
        return new_strategy_group_id


    def calculate_gt_data_hash(self):
        openedFile = open(self.gt_path)
        readFile = openedFile.read()

        sha1Hash = hashlib.sha1(readFile.encode('utf-8'))
        sha1Hashed = sha1Hash.hexdigest()
        return sha1Hashed

    def get_existing_frames(self):
        existing_frames = self.db_mngr.get_existing_frames(self.gt_path)
        ex_frames = {}
        for tup in existing_frames:
            ex_frames[tup[1]] = {
                'frame_id': tup[0],
                'modified_date': tup[2],
                'status_id': tup[3],
                'strategy_id': tup[4],
                'total_num_objects': tup[5]
            }
        return ex_frames

    def process_frames(self):
        frames = {}
        annotated_frames = []
        for fr in self.gt_data_dict['AnnotatedFrames']:
            annotated_frames.append(fr)
        if 'FramesStatuses' in self.gt_data_dict.keys():
            for gt_status in self.gt_data_dict['FramesStatuses']:
                for fr in self.gt_data_dict['FramesStatuses'][gt_status]:
                    annotated_frames.append(fr)
        annotation_source = "internal"
        if "playment" in self.gt_path:
            annotation_source = "playment"
        if 'Strategies' in self.gt_data_dict.keys():
            if len(self.gt_data_dict['Strategies'].keys()) > 0:
                self.strategy = self.get_strategy_id(self.gt_data_dict['Strategies'])
        for tracklet in self.gt_data_dict['Tracklets'].values():
            for pose in tracklet['Poses']:
                if pose['FrameNo'] in annotated_frames:
                    if pose['FrameNo'] not in frames.keys():
                        self.frameStatus = 9 #Unknown
                        if 'FramesStatuses' in self.gt_data_dict.keys():
                            for gt_status in self.gt_data_dict['FramesStatuses']:
                                if pose['FrameNo'] in self.gt_data_dict['FramesStatuses'][gt_status]:
                                    for db_status in self.db_mngr.statuses:
                                        if gt_status.upper() == db_status[1].upper():
                                            self.frameStatus = db_status[0]
                        frames[pose['FrameNo']] = Frame(tracklet['UpdatedTime'][:19],
                                                        tracklet['CreatedTime'][:19],
                                                        self.hash,
                                                        self.frameStatus,
                                                        self.strategy,
                                                        self.gt_path,
                                                        self.sensor_suite_id,
                                                        pose['FrameNo'],
                                                        annotation_source)
                    curFrame = frames[pose['FrameNo']]
                    if tracklet['UpdatedTime'][:19] > curFrame.modified_date:
                       curFrame.modified_date = tracklet['UpdatedTime'][:19]
                    if tracklet['CreatedTime'][:19] > curFrame.created_date:
                       curFrame.created_date = tracklet['CreatedTime'][:19]
                    type_str = curFrame.type_to_str[tracklet['Type']]
                    setattr(curFrame, type_str, getattr(curFrame, type_str) + 1)
                    curFrame.total_num_objects += 1
        for frameId in set(annotated_frames):
            if frameId not in frames.keys():
                cur_frame_status = 9  # Unknown
                if 'FramesStatuses' in self.gt_data_dict.keys():
                    for gt_status in self.gt_data_dict['FramesStatuses']:
                        if frameId in self.gt_data_dict['FramesStatuses'][gt_status]:
                            for db_status in self.db_mngr.statuses:
                                if gt_status == db_status[1]:
                                    cur_frame_status = db_status[0]
                frames[frameId] = Frame(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
                                        datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
                                        self.hash,
                                        cur_frame_status,
                                        self.strategy,
                                        self.gt_path,
                                        self.sensor_suite_id,
                                        frameId,
                                        annotation_source)

        return frames
        #list(json_data["Tracklets"].values())

    def create_frame(self, frame):
        frame_obj = {       "modified_date": frame.modified_date,
                            "created_date": frame.created_date,
                            "gt_file_path": frame.gt_path,
                            "sensor_suite_id": frame.sensor_suite_id,
                            "frame_number": frame.frame_number,
                            "strategy_id": frame.strategy,
                            "status_id": frame.status,
                            "description": "",
                            "annotation_source": frame.annotation_source,
                            "hash": frame.hash
        }
        object_id = self.db_mngr.insert_obj("od_frames", frame_obj);
        return object_id

    def update_frame(self, cur_id, frame):
        frame_obj = {       "modified_date": frame.modified_date,
                            "created_date": frame.created_date,
                            "strategy_id": frame.strategy,
                            "status_id": frame.status,
                            "description": "",
                            "annotation_source": frame.annotation_source,
                            "hash": frame.hash,
                            "id": cur_id
        }
        object_id = self.db_mngr.update_frame(frame_obj);
        return object_id

    def mark_frame_deleted(self, cur_id):
        frame_summary = {
            "modified_date": datetime.now(),
            "frame_id": cur_id,
            "status_id": 9,
        }
        object_id = self.db_mngr.insert_obj("od_frame_object_summary", frame_summary);
        self.db_mngr.mark_frame_deleted(cur_id);

    def create_frame_status(self, frame, frame_db_id):
        frame_summary = {
                "modified_date": frame.modified_date,
                "frame_id": frame_db_id,
                "total_num_objects": frame.total_num_objects,
                "status_id": frame.status,
                "pedestrian": frame.pedestrian,
                "car": frame.car,
                "van": frame.van,
                "cyclist": frame.cyclist,
                "truck": frame.truck,
                "tram": frame.tram,
                "person": frame.person,
                "misc": frame.misc,
                "ignored": frame.ignore,
                "noise": frame.noise,
                "motorcycle": frame.motorcycle,
                "animal": frame.animal,
                "motorcyclist": frame.motorcyclist,
                "bicycle": frame.bicycle,
                "wheelchair": frame.wheelchair,
                "stroller": frame.stroller,
                "trailer": frame.trailer,
                "scooter": frame.scooter,
                "kalnoit": frame.kalnoit,
                "ghost": frame.ghost,
                "unknown": frame.unknown,
                "edgecase": frame.edgecase,
                "sittingperson": frame.sittingperson
        }
        object_id = self.db_mngr.insert_obj("od_frame_object_summary", frame_summary);
        return object_id