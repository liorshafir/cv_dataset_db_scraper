import os
import json
from datetime import datetime

from logging import FATAL

def log_default(msg, level):
    print(f"[{level}] [{msg}]")


class DirProcess:
    def __init__(self, dir_path, db_mngr, log_cbk=None):
        self.dir_path = dir_path
        try:
            recording_date = self.returnDate(dir_path).strftime('%Y-%m-%d %H:%M:%S')
            self.created_date = recording_date
            self.modified_date = recording_date
        except:
            self.created_date = datetime.fromtimestamp(os.path.getctime(dir_path)).strftime('%Y-%m-%d %H:%M:%S')
            self.modified_date = datetime.fromtimestamp(os.path.getmtime(dir_path)).strftime('%Y-%m-%d %H:%M:%S')
        self.db_mngr = db_mngr
        self.directory_list = None

        if log_cbk is None:
            self._log_cbk = log_default
        else:
            self._log_cbk = log_cbk

        if "innoviz_dataset\\_2018_07_new_mpu" in self.dir_path or "innoviz_dataset_Trucks" in self.dir_path or "two-wheeled" in self.dir_path:
            self.recording_tool_ver = "SensorPanelV0"
        else:
            self.recording_tool_ver = "RosV1"

        # Fields enter in the database
        self.is_setup_configuration = False
        self.is_camera_file = False
        self.is_ins_file = False
        self.is_lidar_pro = False
        self.is_lidar_one = False
        self.configuration_path = None
        self.tstream_path_lidar = None
        self.tstream_path_camera = None
        self.path_lidar_pro = None
        self.path_lidar_one = None
        self.format_lidar_pro = None
        self.format_lidar_one = None
        self.path_camera = None
        self.format_camera = None
        self.path_ins = None
        self.format_ins = None
        self.file_pro_invz = None
        self.file_one_invz = None
        self.camera_num_frames = None
        self.lidar_pro_num_frames = None
        self.lidar_one_num_frames = None
        self.module_type_camera = None
        self.time_camera = None
        self.time_lidar_pro = None
        self.time_lidar_one = None
        self.camera_duration_secs = None
        self.lidar_pro_duration_secs = None
        self.lidar_one_duration_secs = None
        self.camera_file_length = None
        self.lidar_pro_file_length = None
        self.lidar_one_file_length = None
        self.ins_file_length = None
        self.path_metadata = None
        self.invz_pro_created_date = None
        self.invz_pro_modified_date = None
        self.invz_one_created_date = None
        self.invz_one_modified_date = None
        self.camera_created_date = None
        self.camera_modified_date = None
        self.ins_created_date = None
        self.ins_modified_date = None


    def log(self, msg, level):
        self._log_cbk(msg, level)

    def run(self):
        self.directory_list = os.listdir(self.dir_path)
        # load the parameters
        self.pass_on_files(self.directory_list)
        # create recording session
        recording_session_id = self.create_rec_session()
        # create sensor suite
        sensor_suite_id = self.create_sensor_suite(recording_session_id)
        self.db_mngr.insert_change_record(datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "NEW_RECORDING", None, sensor_suite_id, "A new recording folder was found at: " + self.dir_path)

        # create raw module data
        self.create_raw_module_datas(sensor_suite_id)
        return sensor_suite_id

    def create_rec_session(self):

        rec_session_obj = { "modified_date": self.modified_date,
                            "recording_date": self.created_date,
                            "source_type": "file_system_db_scraper",
                            "stage": 150,
                            "path": self.dir_path,
                            "meta_data_path": self.path_metadata,
                            "status": "Recorded"
                           }
        object_id = self.db_mngr.insert_obj("recording_sessions", rec_session_obj);
        return object_id

    def create_sensor_suite(self, recording_session_id):
        sensor_suite_obj = { "modified_date": self.modified_date,
                             "created_date": self.created_date,
                             "recording_session_id" : recording_session_id,
                             "recording_tool_ver" : self.recording_tool_ver,
                             "data_status": "Recorded",
                             "description": "Front sensors"
                            }
        object_id = self.db_mngr.insert_obj("sensor_suites", sensor_suite_obj);
        return object_id

    def create_raw_module_datas(self, sensor_suite_id):
        try:
            data = open(self.dir_path + "\\metadata.json").read()
            metadata = json.loads(data)
            module_fw_version = metadata["Attributes"]["Version"]
            module_type_lidar = metadata["Attributes"]["Device Name"]
            module_serial = metadata["Attributes"]["Serial Number"]
        except:
            module_fw_version = None
            module_type_lidar = None
            module_serial = None

        if (self.is_lidar_pro):
            lidar_pro_raw_data_obj = {
                                   "modified_date": self.invz_pro_modified_date,
                                   "created_date": self.invz_pro_created_date,
                                   "sensor_suite_id": sensor_suite_id,
                                   "path": self.path_lidar_pro,
                                   "tstream_path": self.tstream_path_lidar,
                                   "source_type": "file_system_db_scraper",
                                   "module_fw_version": module_fw_version,
                                   "module_type": module_type_lidar,
                                   "module_serial": module_serial,
                                   "sensor_type": "PRO",
                                   "format": self.format_lidar_pro,
                                   "configuration_path": self.configuration_path,
                                   "intrinsic_calib_path": self.configuration_path,
                                   "num_frames": self.lidar_pro_num_frames,
                                   "duration": self.lidar_pro_duration_secs,
                                   "raw_file_length": self.lidar_pro_file_length
                                }

            self.db_mngr.insert_obj("raw_module_data", lidar_pro_raw_data_obj);

        if (self.is_lidar_one):
            lidar_one_raw_data_obj = {
                                   "modified_date": self.invz_one_modified_date,
                                   "created_date": self.invz_one_created_date,
                                   "sensor_suite_id": sensor_suite_id,
                                   "path": self.path_lidar_one,
                                   "tstream_path": self.tstream_path_lidar,
                                   "source_type": "file_system_db_scraper",
                                   "module_fw_version": module_fw_version,
                                   "module_type": module_type_lidar,
                                   "module_serial": module_serial,
                                   "sensor_type": "ONE",
                                   "format": self.format_lidar_one,
                                   "configuration_path": self.configuration_path,
                                   "intrinsic_calib_path": self.configuration_path,
                                   "num_frames": self.lidar_one_num_frames,
                                   "duration": self.lidar_one_duration_secs,
                                   "raw_file_length": self.lidar_one_file_length
                                }

            self.db_mngr.insert_obj("raw_module_data", lidar_one_raw_data_obj);


        if self.is_camera_file:
            camera_raw_data_obj = {
                                   "modified_date": self.camera_modified_date,
                                   "created_date": self.camera_created_date,
                                   "sensor_suite_id": sensor_suite_id,
                                   "path": self.path_camera,
                                   "tstream_path": self.tstream_path_lidar,
                                   "source_type": "file_system_db_scraper",
                                   "module_fw_version": module_fw_version,
                                   "module_type": module_type_lidar,
                                   "module_serial": module_serial,
                                   "sensor_type": "BESELER",
                                   "format": self.format_camera,
                                   "configuration_path": self.configuration_path,
                                   "intrinsic_calib_path": self.configuration_path,
                                   "num_frames": self.camera_num_frames,
                                   "duration": self.camera_duration_secs,
                                   "raw_file_length": self.camera_file_length
                                }
            self.db_mngr.insert_obj("raw_module_data", camera_raw_data_obj);

        if self.path_ins is not None:
            ins_raw_data_obj = {
                                   "modified_date": self.ins_modified_date,
                                   "created_date": self.ins_created_date,
                                   "sensor_suite_id": sensor_suite_id,
                                   "path": self.path_ins,
                                   "tstream_path": None,
                                   "source_type": "file_system_db_scraper",
                                   "module_fw_version": None,
                                   "module_type": None,
                                   "module_serial": None,
                                   "sensor_type": "INS",
                                   "format": self.format_ins,
                                   "configuration_path": self.configuration_path,
                                   "intrinsic_calib_path": self.configuration_path,
                                   "num_frames": None,
                                   "duration": None,
                                   "raw_file_length": self.ins_file_length
                                   }
            self.db_mngr.insert_obj("raw_module_data", ins_raw_data_obj);

    def returnDate(self, dir_path):
        x = []
        x = dir_path.split('\\')
        dir = x[-1]
        y = dir.split('_')
        year = y[0][0:4]
        month = y[0][5:7]
        day = y[0][8:10]

        hour = y[1][0:2]
        minute = y[1][2:4]
        second = y[1][4:6]
        return datetime(int(year), int(month), int(day), int(hour), int(minute), int(second))

    def pass_on_files(self, directory_list):
        for file in directory_list:
            if file == "config" and os.path.isfile(self.dir_path + '\\config\\setup.json'):
                self.configuration_path = self.dir_path + '\\config\\setup.json'
                self.is_setup_configuration = True
            if file == "calibration.json" and not self.is_setup_configuration:
                self.configuration_path = self.dir_path + '\\calibration.json'
            if file.endswith('.indx'):
                self.tstream_path_lidar = self.dir_path + "\\"+ file
            if file.startswith("metadata"):
                self.path_metadata = self.dir_path + '\\' + file
            if file.endswith('.invz4_3'):
                self.is_lidar_one = True
                self.path_lidar_one = self.dir_path + '\\' + file
                self.format_lidar_one = "INVZ4_3"
                invz4_path = os.path.join(self.dir_path, file)
                self.invz_one_created_date = datetime.fromtimestamp(os.path.getctime(invz4_path)).strftime('%Y-%m-%d %H:%M:%S')
                self.invz_one_modified_date = datetime.fromtimestamp(os.path.getmtime(invz4_path)).strftime('%Y-%m-%d %H:%M:%S')
                self.lidar_one_file_length = sum([os.path.getsize(os.path.join(invz4_path, f)) for f in os.listdir(invz4_path) if os.path.isfile(os.path.join(invz4_path, f))])
                self.file_one_invz =  self.dir_path + '\\' + file
            if file.endswith('.invz'):
                self.is_lidar_pro = True
                self.path_lidar_pro = self.dir_path + '\\' + file
                self.format_lidar_pro = "INVZ2"
                self.invz_pro_created_date = datetime.fromtimestamp(os.path.getctime(os.path.join(self.dir_path, file))).strftime('%Y-%m-%d %H:%M:%S')
                self.invz_pro_modified_date = datetime.fromtimestamp(os.path.getmtime(os.path.join(self.dir_path, file))).strftime('%Y-%m-%d %H:%M:%S')
                self.lidar_pro_file_length = os.path.getsize(os.path.join(self.dir_path, file))
                self.file_pro_invz =  self.dir_path + '\\' + file
            if file.endswith('.avi'):
                if (not self.is_camera_file) or file.__contains__("synched"):
                    self.is_camera_file = True
                    self.path_camera = self.dir_path + '\\' + file
                    self.camera_file_length = os.path.getsize(os.path.join(self.dir_path, file))
                    self.camera_created_date = datetime.fromtimestamp(os.path.getctime(os.path.join(self.dir_path, file))).strftime('%Y-%m-%d %H:%M:%S')
                    self.camera_modified_date = datetime.fromtimestamp(os.path.getmtime(os.path.join(self.dir_path, file))).strftime('%Y-%m-%d %H:%M:%S')
                    self.format_camera = "AVI"
            if file.endswith('.tstream') and file.__contains__("Camera"):
                self.tstream_path_camera =  self.dir_path + '\\' + file
                try:
                    num_lines = sum(1 for line in open(self.tstream_path_camera))
                    self.camera_num_frames = num_lines - 1
                    self.camera_duration_secs = self.camera_num_frames / 16
                except Exception as a:
                    self.log(a, FATAL)
                self.module_type_camera = 'Camera'
            if file.endswith('.tstream') and file.__contains__("Lidar") or file.endswith(".invz.index"):
                self.tstream_path_lidar =  self.dir_path + '\\' + file
                try:
                    num_lines = sum(1 for line in open(self.tstream_path_lidar))
                    self.lidar_pro_num_frames = num_lines - 1
                    self.lidar_pro_duration_secs = self.lidar_pro_num_frames / 16
                    self.lidar_one_num_frames = num_lines - 1
                    self.lidar_one_duration_secs = self.lidar_one_num_frames / 15
                except Exception as a:
                    self.log(a, FATAL)
            if file.__contains__("ins"):
                self.path_ins = self.dir_path + '\\' + file
                self.format_ins = 'ostream'
                self.ins_file_length = os.path.getsize(os.path.join(self.dir_path, file))
                self.ins_created_date = datetime.fromtimestamp(os.path.getctime(os.path.join(self.dir_path, file))).strftime(
                    '%Y-%m-%d %H:%M:%S')
                self.ins_modified_date = datetime.fromtimestamp(os.path.getmtime(os.path.join(self.dir_path, file))).strftime(
                    '%Y-%m-%d %H:%M:%S')


class DatetimeEncoder(json.JSONEncoder):
    def default(self, obj):
        try:
            return super(DatetimeEncoder, obj).default(obj)
        except TypeError:
            return str(obj)

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        try:
            return super(JSONEncoder, obj).default(obj)
        except TypeError:
            return str(obj)