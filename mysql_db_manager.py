import scraper_config as cfg
import mysql.connector


def log_default(msg, level):
    print(f"[{level}] msg")


class MySQLManager:
    def __init__(self, log_cbk=None):
        # set log callback
        if log_cbk is None:
            self._log_cbk = log_default
        else:
            self._log_cbk = log_cbk

    def connect(self):
        self.mydb = mysql.connector.connect(
            host=cfg.db_config['host'],
            database=cfg.db_config['database'],
            user=cfg.db_config['user'],
            password=cfg.db_config['password'],
            auth_plugin=cfg.db_config['auth_plugin']
        )
        self.mydb = self.mydb

    def log(self, msg, level):
        self._log_cbk(msg, level)

    def get_static_metadata(self):
        self.statuses = self.get_statuses()
        self.statusesDict = {}
        for status in self.statuses:
            self.statusesDict[status[0]] = status[1]
        str_items = self.get_strategy_items()
        self.strategy_items = {}
        for st_item in str_items:
            self.strategy_items[st_item[0]] = {
                                                'key': st_item[1],
                                                'type': st_item[2],
                                                'value': st_item[3]
                                                }
        str_groups = self.get_strategy_groups()
        self.strategy_groups = {}
        for stgp in str_groups:
            if not stgp[0] in self.strategy_groups.keys():
                self.strategy_groups[stgp[0]] = []
            self.strategy_groups[stgp[0]].append(stgp[1])

    def get_statuses(self):
        try:
            mycursor = self.mydb.cursor()
            mycursor.execute("SELECT id, status FROM od_frame_status")
            myresult = mycursor.fetchall()
        except mysql.connector.Error as error:
            self.log("Error getting statuses", error);
        return myresult

    def get_existing_recording_paths(self):
        try:
            mycursor = self.mydb.cursor()
            mycursor.execute("SELECT distinct ses.id, path from recording_sessions rs INNER JOIN sensor_suites ses ON rs.id = ses.recording_session_id")
            myresult = mycursor.fetchall()
        except mysql.connector.Error as error:
            self.log("Error getting recording paths", error);
        return myresult

    def get_existing_gt_files(self):
        try:
            mycursor = self.mydb.cursor()
            mycursor.execute("SELECT gt_file_path, modified_date, hash from od_file_tracking")
            myresult = mycursor.fetchall()
        except mysql.connector.Error as error:
            self.log("Error getting statuses", error);
        return myresult

    def get_existing_frames(self, gt_path):
        try:
            mycursor = self.mydb.cursor()
            mytuple = tuple(list([gt_path]))
            mycursor.execute("SELECT odf.id, odf.frame_number, odf.modified_date, odf.status_id, odf.strategy_id, ofos.total_num_objects from od_frames odf INNER JOIN od_frame_object_summary ofos ON odf.id = ofos.frame_id WHERE odf.gt_file_path like %s ESCAPE \'|\' AND odf.status_id != 9 AND ofos.modified_date = (select max(modified_date) from od_frame_object_summary where frame_id = odf.id)", mytuple)
            myresult = mycursor.fetchall()
        except mysql.connector.Error as error:
            self.log("Error getting existing frames", error)
        return myresult

    def insert_strategy_item(self, strategy_key, strategy_value):
        try:
            mycursor = self.mydb.cursor()
            mySql_insert_query = "INSERT INTO od_strategies_items(strategy_key, strategy_value, description) VALUES (%s, %s, %s)"
            mytuple = tuple(list([strategy_key, strategy_value, "Added by update DB script"]))
            result = mycursor.execute(mySql_insert_query, mytuple)
            self.mydb.commit()
            return mycursor.lastrowid

        except mysql.connector.Error as error:
            self.log("Error inserting new strategy item", error)

        return -1

    def insert_strategy_group(self, ids):
        try:
            max_id = max(self.strategy_groups.keys())

            for newid in ids:
                mycursor = self.mydb.cursor()
                mySql_insert_query = "INSERT INTO od_strategy_groups(id, strategy_item_id) VALUES (%s, %s)"
                mytuple = tuple(list([max_id + 1, newid]))
                result = mycursor.execute(mySql_insert_query, mytuple)
                self.mydb.commit()

            return max_id + 1
        except mysql.connector.Error as error:
            self.log("Error inserting new strategy item", error)

        return -1

    def insert_change_record(self, change_date, change_type, frame_id, sensor_suite_id, description):
        try:
            mycursor = self.mydb.cursor()
            mySql_insert_query = "INSERT INTO changes_log (change_date, change_type, frame_id, sensor_suite_id, description) VALUES (%s, %s, %s, %s, %s)"
            mytuple = tuple(list([change_date, change_type, frame_id, sensor_suite_id, description]))
            result = mycursor.execute(mySql_insert_query, mytuple)
            self.mydb.commit()
        except mysql.connector.Error as error:
            self.log("Error inserting change", error)

    def update_file_tracking(self, path, modified, hash):
        try:
            mycursor = self.mydb.cursor()
            mySql_update_query = "UPDATE od_file_tracking SET modified_date = %s, hash = %s WHERE id > 0 AND gt_file_path = %s"
            mytuple = tuple(list([modified, hash, path]))
            result = mycursor.execute(mySql_update_query, mytuple)
            self.mydb.commit()
        except mysql.connector.Error as error:
            self.log("Error updating statuses", error)

    def create_file_tracking(self, path, modified, hash):
        try:
            mycursor = self.mydb.cursor()
            mySql_insert_query = "INSERT INTO od_file_tracking (modified_date, hash, gt_file_path) VALUES (%s, %s, %s)"
            mytuple = tuple(list([modified, hash, path]))
            result = mycursor.execute(mySql_insert_query, mytuple)
            self.mydb.commit()
        except mysql.connector.Error as error:
            self.log("Error inserting file tracking", error)

    def get_strategy_items(self):
        try:
            mycursor = self.mydb.cursor()
            mycursor.execute("SELECT id, strategy_key, strategy_type, strategy_value FROM od_strategies_items")
            myresult = mycursor.fetchall()
        except mysql.connector.Error as error:
            self.log("Error getting strategy items", error)
        return myresult

    def get_strategy_groups(self):
        try:
            mycursor = self.mydb.cursor()
            mycursor.execute("SELECT id, strategy_item_id FROM od_strategy_groups")
            myresult = mycursor.fetchall()
        except mysql.connector.Error as error:
            self.log("Error getting strategy groups", error)
        return myresult

    def insert_ignore_file(self, file_name):
        try:
            mycursor = self.mydb.cursor()
            mytuple = tuple(list(['0000-00-00', file_name, 1]))
            mySql_insert_query = "INSERT INTO od_file_tracking ( modified_date, gt_file_path, is_ignore) VALUES (%s, %s, %s)"
            result = mycursor.execute(mySql_insert_query, mytuple)
            self.mydb.commit()

        except mysql.connector.Error as error:
            self.log("Error inserting new ignore file", error);

    def update_frame(self, obj):
        try:
            mycursor = self.mydb.cursor()
            mytuple = tuple(list(obj.values()))
            mySql_update_query = "UPDATE od_frames SET modified_date = %s, created_date = %s, strategy_id = %s, status_id = %s, description = %s, annotation_source = %s, hash = %s WHERE id = %s "

            result = mycursor.execute(mySql_update_query, mytuple)
            self.mydb.commit()

            return mycursor.lastrowid

        except mysql.connector.Error as error:
            self.log("Error inserting new recording", error);

        return -1;

    def mark_frame_deleted(self, cur_id):
        try:
            mycursor = self.mydb.cursor()
            mySql_update_query = "UPDATE od_frames SET status_id = 9 WHERE id = " + str(cur_id)

            result = mycursor.execute(mySql_update_query)
            self.mydb.commit()

            return mycursor.lastrowid

        except mysql.connector.Error as error:
            self.log("Error updating frame as deleted", error);
        return -1;

    def insert_obj(self, table_name, obj):
        try:
            mycursor = self.mydb.cursor()
            mytuple = tuple(list(obj.values()))
            mySql_insert_query = "INSERT INTO " + table_name + "(" + ','.join(obj.keys()) + """)
                                                    VALUES (""" + ','.join(["%s" for item in obj.keys()]) + ") "
            result = mycursor.execute(mySql_insert_query, mytuple)
            self.mydb.commit()

            return mycursor.lastrowid

        except mysql.connector.Error as error:
            self.log("Error inserting new recording", error);

        return -1;

    def insert_sensor_suite(self, params):
        try:
            mySql_insert_query = """INSERT INTO sensor_suites (modified_date, created_date, recording_session_id, data_status, description) 
                                    VALUES (%s, %s, %s, %s, %s, %s) """
        except mysql.connector.Error as error:
            self.log("Error inserting new recording", error);

        return obj;

    def update_deleted_sensor_suite(self, sensor_suite_id):
        try:
            mycursor = self.mydb.cursor()
            mySql_update_query = "UPDATE sensor_suites SET data_status = 'Deleted' WHERE id = " + str(sensor_suite_id)

            result = mycursor.execute(mySql_update_query)
            self.mydb.commit()

            return mycursor.lastrowid

        except mysql.connector.Error as error:
            self.log("Error updating sensor suite as deleted", error);
        return -1;

    def insert_raw_file(self, params):
        try:
            mySql_insert_query = """INSERT INTO raw_module_datas (modified_date, created_date, sensor_suite_id, path, tstream_path, source_type,
            module_fw_version, module_type, module_serial, format, configuration_path, intrinsic_calib_path, num_frames, duration, raw_file_length) 
                                    VALUES (%s, %s, %s, %s, %s, %s, %s) """
        except mysql.connector.Error as error:
            self.log("Error inserting new recording", error);

        return obj;

